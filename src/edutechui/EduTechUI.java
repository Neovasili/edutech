/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edutechui;

import java.awt.Dimension;
import java.awt.Toolkit;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.fazecast.jSerialComm.SerialPortPacketListener;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.fazecast.jSerialComm.SerialPortPacketListener;
import javafx.stage.StageStyle;

/**
 *
 * @author Alberto
 */
public class EduTechUI extends Application {

    @Override
    public void start(Stage stage) throws Exception {
//        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLDocument.fxml"));
        Parent root = loader.load();
        FXMLDocumentController c = loader.getController();

        SerialPort comPort = getEduTechSerialPort();
        if (comPort == null) {
            System.exit(0);
        }

        comPort.openPort();
        comPort.addDataListener(c);

        Toolkit t = Toolkit.getDefaultToolkit(); // instancia a la clase Toolkit. Utilizadapara obtener resolucion de pantalla
        Dimension screenSize = t.getScreenSize(); //Obtiene del método las dimensiones de la pantalla.

        System.out.println("Tu resolución es de " + screenSize.width + " x " + screenSize.height); //Imprime las dim de la pantalla

//        stage.setWidth(screenSize.getWidth());
//        stage.setHeight(screenSize.getHeight() * 0.9);
        stage.setWidth(1280);
        stage.setHeight(768);
        stage.initStyle( StageStyle.UNDECORATED );
        // Hacer que la pantalla no tenga la barra superior de windows

        // Escalar los botones
        // Largar la app en preguntas
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

// devuelve el puerot donde esta conectado el eduTeh o null si no hay ningunl
    SerialPort getEduTechSerialPort() {
        SerialPort comPorts[] = SerialPort.getCommPorts();
        for (int i = 0; i < comPorts.length; i++) {
            SerialPort comPort = comPorts[i];
            comPort.openPort();
            comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 100, 0);

            try {

                int cnt = 0;
                final int limite = 20; // tiempo durante el cual verifica si esta conectado EduTech en cada puerto serie
                while (true) {

                    byte[] readBuffer = new byte[1024];
                    int numRead = comPort.readBytes(readBuffer, readBuffer.length);
                    if (numRead == 0) {
                        if (cnt > limite) {
                            System.out.println("No hay EduTech en " + comPort.getSystemPortName());
                            cnt = 0;
                            comPort.closePort();
                            break;
                        }
                        cnt++;
                    } else {
                        System.out.println("EduTech en " + comPort.getSystemPortName());
                        cnt = 0;
                        comPort.closePort();
                        return comPort;
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            comPort.closePort();
        }
        return null;
    }

}
